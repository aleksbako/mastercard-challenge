package com.transferSystem.MasterCardChallenge.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class StatementTest {
    private Statement statement;
    @BeforeEach
    void SetUp(){
        statement = new Statement(1,2,"EURO","DEBIT", LocalDateTime.now());
    }
    @Test
    void TestSetandGetAccountid() {
        statement.setAccountid(2);
        assertNotEquals(1,statement.getAccountid());
    }

    @Test
    void TestSetandGetAmount() {
        statement.setAmount(500);
        assertNotEquals(2,statement.getAmount());
    }

    @Test
    void TestSetandGetCurrency() {
        statement.setCurrency("GBP");
        assertNotEquals(1,statement.getCurrency());
    }

    @Test
    void TestSetandGetType() {
        statement.setType("CREDIT");
        assertNotEquals("DEBIT",statement.getType());
    }

    @Test
    void TestSetandGetTransactionDate() {
        LocalDateTime time = statement.getTransactionDate();
        statement.setTransactionDate(LocalDateTime.now());
        assertNotEquals(time,statement.getTransactionDate());
    }
}