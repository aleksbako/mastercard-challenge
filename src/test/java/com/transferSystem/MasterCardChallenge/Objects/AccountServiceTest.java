package com.transferSystem.MasterCardChallenge.Objects;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccountServiceTest {

    @Test
    void AddAndGetAccount() {
        Account account = new Account(0,20, "EURO");
        AccountService.addAccount(account);
        assertEquals(account.getAccountid(),AccountService.getAccountById(0).getAccountid());
        assertEquals(account.getBalance().getBalance(),AccountService.getAccountById(0).getBalance().getBalance());
        assertEquals(account.getBalance().getCurrency(),AccountService.getAccountById(0).getBalance().getCurrency());
    }

    @Test
    void AddNewAccountWithSameId() {
        Account account1 = new Account(1,20, "EURO");
        Account account2 = new Account(1,35, "GBP");
        AccountService.addAccount(account1);
        AccountService.addAccount(account2);
        assertEquals(account2.getAccountid(),AccountService.getAccountById(1).getAccountid());
        assertNotEquals(account2.getBalance().getBalance(),AccountService.getAccountById(1).getBalance().getBalance());
        assertNotEquals(account2.getBalance().getCurrency(),AccountService.getAccountById(1).getBalance().getCurrency());
    }
}