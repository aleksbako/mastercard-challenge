package com.transferSystem.MasterCardChallenge.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AccountTest {
    private Account account;
    @BeforeEach
    void SetUp(){
        account = new Account(1,20,"EURO");
    }
    @Test
    void TestgetMiniWithNoStatements() {
        assertTrue(account.getMini().length == 0);
    }

    @Test
    void TestgetMiniWithOneStatements(){
        Statement statement = new Statement(1,1,"EURO","DEBIT", LocalDateTime.now());
        account.addNewStatement(statement);
        assertTrue(account.getMini().length == 1);
    }

    @Test
    void TestgetMiniWithManyStatements(){
        for(int i = 0; i < 10;i++) {
            account.addNewStatement(new Statement(1, 1, "EURO", "DEBIT", LocalDateTime.now()));
        }
        assertTrue(account.getMini().length == 10);
    }
    @Test
    void TestGetAccountid() {
        assertEquals(1,account.getAccountid());
    }

    @Test
    void TestSetandGetBalance() {
        account.getBalance().setBalance(50);
        assertNotEquals(20,account.getBalance().getBalance());
    }

    @Test
    void TestSetAndGetStatements() {
        Stack<Statement> statementStack = new Stack<>();
        statementStack.push(new Statement(1,1,"EURO","DEBIT",LocalDateTime.now()));
        assertEquals(0,account.getStatements().size());
        account.setStatements(statementStack);
        assertNotEquals(0,account.getStatements().size());
    }



    @Test
    void addNewStatementDEBIT() {


        Statement statement = new Statement(1,2,"EURO","DEBIT", LocalDateTime.now());
        account.addNewStatement(statement);
        assertTrue(account.getBalance().getBalance() <20.0);
        assertEquals(1,account.getStatements().size());
    }
    @Test
    void addNewStatementCREDIT() {


        Statement statement = new Statement(1,2,"EURO","CREDIT", LocalDateTime.now());
        account.addNewStatement(statement);
        assertTrue(account.getBalance().getBalance() >20.0);
        assertEquals(1,account.getStatements().size());
    }
}