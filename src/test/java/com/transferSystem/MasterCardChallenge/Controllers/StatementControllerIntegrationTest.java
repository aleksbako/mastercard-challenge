package com.transferSystem.MasterCardChallenge.Controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.transferSystem.MasterCardChallenge.Errors.InsufficientFunds;
import com.transferSystem.MasterCardChallenge.Errors.InvalidAccountNumber;
import com.transferSystem.MasterCardChallenge.Objects.Account;
import com.transferSystem.MasterCardChallenge.Objects.AccountService;
import com.transferSystem.MasterCardChallenge.Objects.Statement;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;



import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(StatementController.class)
public class StatementControllerIntegrationTest {
    @Autowired
    private MockMvc mvc;

    @Test
    void CreateStatementInvalidAccountNumber() throws Exception {
        Statement statement = new Statement(2,21,"EURO","CREDIT",LocalDateTime.now());

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        RequestBuilder request = MockMvcRequestBuilders.post("/accounts/{id}/statements/create",5)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(statement));

        mvc.perform(request).
                andExpect(status().isBadRequest()).
                andExpect(result -> assertTrue(result.getResolvedException() instanceof InvalidAccountNumber));
    }
    @Test
    void CreateStatementInsufficientFunds() throws Exception {
        AccountService.addAccount(new Account(-1,0,"EURO"));
        AccountService.addAccount(new Account(2,30,"EURO"));
        Statement statement = new Statement(2,21,"EURO","DEBIT",LocalDateTime.now());

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        RequestBuilder request = MockMvcRequestBuilders.post("/accounts/{id}/statements/create",-1)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(statement));

        mvc.perform(request).
                andExpect(status().isBadRequest()).
                andExpect(result -> assertTrue(result.getResolvedException() instanceof InsufficientFunds));
    }


    @Test
    void TestCreateStatementWithInvalidReciever() throws Exception {


        AccountService.addAccount(new Account(1,0,"EURO"));

        Statement statement = new Statement(-1,21,"EURO","DEBIT",LocalDateTime.now());

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        RequestBuilder request = MockMvcRequestBuilders.post("/accounts/{id}/statements/create",1)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(statement));

        mvc.perform(request).
                andExpect(status().isBadRequest()).
                andExpect(result -> assertTrue(result.getResolvedException() instanceof InvalidAccountNumber));
    }

    @Test
    void TestCreateStatementCorrectly() throws Exception {

        AccountService.addAccount(new Account(1,0,"EURO"));
        AccountService.addAccount(new Account(2,30,"EURO"));
        Statement statement = new Statement(2,21,"EURO","DEBIT",LocalDateTime.now());

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        RequestBuilder request = MockMvcRequestBuilders.post("/accounts/{id}/statements/create",1)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(statement));

        MvcResult result = mvc.perform(request).andExpect(status().isOk()).andReturn();
        assertEquals("Transaction completed",result.getResponse().getContentAsString());

    }

    @Test
    void TestgetMiniStatementList() throws Exception {

        AccountService.addAccount(new Account(1,20,"EURO"));
        AccountService.addAccount(new Account(2,30,"EURO"));
        Statement statement = new Statement(2,21,"EURO","DEBIT", LocalDateTime.now());

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        RequestBuilder Postrequest = MockMvcRequestBuilders.post("/accounts/{id}/statements/create",1)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(statement));

        mvc.perform(Postrequest).andExpect(status().isOk());

        RequestBuilder Getrequest = MockMvcRequestBuilders.get("/accounts/{id}/statements/mini",1);
        MvcResult result = mvc.perform(Getrequest).andExpect(status().isOk()).andReturn();
        assertNotNull(result.getResponse().getContentAsString());
    }

    @Test
    void TestgetMiniStatementListInvalidAccount() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders.get("/accounts/{id}/statements/mini",10);
        mvc.perform(request).
                andExpect(status().isBadRequest()).
                andExpect(result -> assertTrue(result.getResolvedException() instanceof InvalidAccountNumber));
    }
}
