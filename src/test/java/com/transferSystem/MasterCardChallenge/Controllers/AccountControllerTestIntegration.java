package com.transferSystem.MasterCardChallenge.Controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.transferSystem.MasterCardChallenge.Errors.InvalidAccountNumber;
import com.transferSystem.MasterCardChallenge.Objects.AccountService;
import com.transferSystem.MasterCardChallenge.Objects.InitializationAccount;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AccountController.class)
class AccountControllerTestIntegration {
    @Autowired
    private MockMvc mvc;

    @Test
    void TestCreateUser() throws Exception {
        InitializationAccount initializationAccount = new InitializationAccount();
        initializationAccount.setAccountid(111);
        initializationAccount.setBalance(3000);
        initializationAccount.setCurrency("GBP");
        ObjectMapper objectMapper = new ObjectMapper();

            RequestBuilder request = MockMvcRequestBuilders.post("/accounts/createAccount")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(initializationAccount));

        MvcResult result = mvc.perform(request).andReturn();
        assertEquals("Account Successfully created",result.getResponse().getContentAsString());
        assertEquals(111,AccountService.getAccountById(111).getAccountid());
        assertEquals(3000,AccountService.getAccountById(111).getBalance().getBalance());
        assertEquals("GBP",AccountService.getAccountById(111).getBalance().getCurrency());
    }


    @Test
    void TestAccountBalanceNonExistantId() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders.get("/accounts/{id}/balance",222);
        mvc.perform(request).
                andExpect(status().isBadRequest()).
                andExpect(result -> assertTrue(result.getResolvedException() instanceof InvalidAccountNumber));
    }
    @Test
    void TestAccountBalanceExistantId() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders.get("/accounts/{id}/balance",111);
        mvc.perform(request).
                andExpect(status().isOk()).
                andExpect(result -> assertFalse(result.getResolvedException() instanceof InvalidAccountNumber));
    }
}