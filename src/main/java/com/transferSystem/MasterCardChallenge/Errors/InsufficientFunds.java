package com.transferSystem.MasterCardChallenge.Errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
/**
 * InsufficientFunds error occours when Account attemnts to perform a transaction with 0 balance.
 * Provides a httpstatus of bad request when occoured.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InsufficientFunds extends Exception{
}
