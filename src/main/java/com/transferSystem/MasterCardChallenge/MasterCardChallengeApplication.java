package com.transferSystem.MasterCardChallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MasterCardChallengeApplication {
	/**
	 * Main starting point to the application.
	 */
	public static void main(String[] args) {
		SpringApplication.run(MasterCardChallengeApplication.class, args);
	}

}
