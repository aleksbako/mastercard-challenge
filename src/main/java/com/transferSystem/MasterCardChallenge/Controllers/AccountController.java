package com.transferSystem.MasterCardChallenge.Controllers;

import com.transferSystem.MasterCardChallenge.Errors.InvalidAccountNumber;
import com.transferSystem.MasterCardChallenge.Objects.Account;
import com.transferSystem.MasterCardChallenge.Objects.AccountService;
import com.transferSystem.MasterCardChallenge.Objects.Balance;
import com.transferSystem.MasterCardChallenge.Objects.InitializationAccount;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/accounts")
/**
 * Account controller handles all requests that are related to handling of Accounts.
 */
public class AccountController {

    @PostMapping("/createAccount")
    /**
     * Creates an account with Initial information provided through InitializationAccount object.
     * @param initializationAccount - the initialization object that provides the basic information needed to start an account.
     * @Returns string of a successful account creation.
     */
    public String createBankAccount(@RequestBody InitializationAccount initializationAccount){
        Account account = new Account(initializationAccount.getAccountid(), initializationAccount.getBalance(), initializationAccount.getCurrency());
        AccountService.addAccount(account);
        return String.format("Account Successfully created");
    }

    @GetMapping("/{id}/balance")
    /**
     * Returns the Balance object in json format for a given account.
     * @id - the account id provided in the request.
     * @retruns - Json object of the accounts balance.
     */
    public Balance getAccountBalance(@PathVariable("id") int id)throws InvalidAccountNumber {
        Account account = AccountService.getAccountById(id);

        if(account == null){
                throw new InvalidAccountNumber();
        }
        else {
            return account.getBalance();
        }
    }



}
