package com.transferSystem.MasterCardChallenge.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Balance object which converts to Json using the jackson library.
 */
public class Balance {
    @JsonProperty("account-id")
    private int accountid;
    @JsonProperty("balance")
    private double balance;
    @JsonProperty("currency")
    private String currency;

    /**
     *
     * @param accountid
     */
    Balance(int accountid, double balance, String currency){
        this.accountid = accountid;
        this.balance = balance;
        this.currency = currency;
    }


    /**
     * Gets the balance of given account-id
     * @return
     */
    public double getBalance() {
        return balance;
    }

    /**
     * Sets new balance of given account-id
     * @param balance - new balance attributes value.
     */
    public void setBalance(double balance) {
        this.balance = balance;
    }

    /**
     * Gets the string type of currency that is used by account holder.
     * @return
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets a new string type of currency used by account holder.
     * @param currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
