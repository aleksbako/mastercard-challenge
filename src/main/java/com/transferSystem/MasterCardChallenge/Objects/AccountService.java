package com.transferSystem.MasterCardChallenge.Objects;


import java.util.HashMap;


/**
 * Serves as in memory storage of all accounts.
 */
public class AccountService {
    private static HashMap<Integer,Account> accounts = new HashMap<>(); //Using hashmaps due to that accountIds can be counted as primary keys. Unique.

    /**
     * Adds the account depending on if the id of the account is already present or not. If it is, then do nothing since Id's are unique.
     * @param account - the account value that will be added to the database.
     */
    public static void addAccount(Account account){
        if(!accounts.containsKey(account.getAccountid())){
            accounts.put(account.getAccountid(),account);
        }
    }

    /**
     *
     * @param id - integer value of the requested account-id.
     * @return - returns Account object of the given id.
     */
    public static Account getAccountById(int id){
        return accounts.get(id);
    }

}
